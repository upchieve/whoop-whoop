import { Selector, t } from 'testcafe'

class LoginPage {
  emailInput: Selector
  passwordInput: Selector
  submitButton: Selector
  forgotPasswordLink: Selector
  loginErrorMessage: Selector

  constructor() {
    this.emailInput = Selector('input')
      .withAttribute('data-whoop-whoop', 'loginPageEmailInput')
    this.passwordInput = Selector('input')
      .withAttribute('data-whoop-whoop', 'loginPagePasswordInput')
    this.submitButton = Selector('button')
      .withAttribute('data-whoop-whoop', 'loginPageSubmitButton')
    this.forgotPasswordLink = Selector('div')
      .withAttribute('data-whoop-whoop', 'loginPageForgotPasswordLink')
    this.loginErrorMessage = Selector('div')
      .withAttribute('data-whoop-whoop', 'loginErrorMessage')
  }

  async fillAndSubmit(email: string, password: string) {
    await t
      .typeText(this.emailInput, email)
      .typeText(this.passwordInput, password)
      .click(this.submitButton)
  }
}

export default new LoginPage()
