import { Selector } from 'testcafe'

class SidebarPage {
  adminLink: Selector

  constructor() {
    this.adminLink = Selector('div')
      .withAttribute('data-whoop-whoop', 'sidebarAdminLink')
  }
}

export default new SidebarPage()
