import { Selector } from 'testcafe'

class VolunteerDashboardPage {
  parentComponent: Selector

  constructor() {
    this.parentComponent = Selector('div')
      .withAttribute('data-whoop-whoop', 'volunteerDashboard')
  }
}

export default new VolunteerDashboardPage()
