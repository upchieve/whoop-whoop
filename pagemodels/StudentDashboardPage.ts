import { Selector } from 'testcafe'

class StudentDashboardPage {
  parentComponent: Selector

  constructor() {
    this.parentComponent = Selector('div')
      .withAttribute('data-whoop-whoop', 'studentDashboard')
  }
}

export default new StudentDashboardPage()
