Feature: Smoke test of critical path

  @CriticalPath @Journey
  Scenario: A student and a volunteer participate in a tutoring session
    * a student logs in
    * the student sees the student dashboard
    * a volunteer logs in
    * the volunteer sees the volunteer dashboard
    * the student makes a request in Algebra 1
    * the student fills out the pre-session survey
    * the student sees the classroom
    * the student sees intro messages from the chatbot
    * the student says "here is my problem" in the chat
    * the student draws on the whiteboard
    * the volunteer sees the student's Algebra 1 request on the volunteer dashboard
    * the volunteer takes the student's Algebra 1 request
    * the volunteer sees the classroom with the student in it
    * the volunteer sees the student's message sent before the volunteer joined
    * the volunteer sees the student's whiteboard drawing created before the volunteer joined
    * the student says "hi" in the chat
    * the volunteer sees the student's message
    * the volunteer says "hi, how can I help you?" in the chat
    * the student sees the volunteer's message
    * the student draws on the whiteboard
    * the volunteer sees the student's drawing
    * the volunteer draws on the whiteboard
    * the student sees the volunteer's drawing
    * the student ends the session
    * the student sees the student post-session survey
    * the student fills and submits out the student post-session survey
    * the volunteer sees the volunteer post-session survey
    * the volunteer fills out and submits the volunteer post-session survey
    * the student sees the student dashboard
    * the volunteer sees the volunteer dashboard
