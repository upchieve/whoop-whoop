Feature: Admins can search and update schools

    As an admin
    I want to search for any school
    and have the ability to make changes to any school

    Rule: Allow admins to change the partner status for any school
        @Admin @School
        Scenario: an admin updates the partner status of a school
            Given an admin has selected a school to edit
            When an admin updates the partner status of that school
            Then there should be a visual indication that the school's partner status was updated



