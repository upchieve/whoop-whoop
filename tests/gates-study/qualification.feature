Feature: Gates study qualification

    # study period = 10/18 - 12/17
    # See glossary for "Gates-qualified student"
    Rule: Students are flagged as Gates-qualified when creating an account within the study period

        @Gates
        Scenario: Student creates an account before the study period
            Given the current date is 10/17
            When a student creates an account
            Then the student is not flagged as Gates-qualified

        @Gates
        Scenario: Student creates an account within the study period
            Given the current date is 10/18
            When a student creates an account
            Then the student is flagged as Gates-qualified

        @Gates
        Scenario: Student creates an account after the study period
            Given the current date is 12/18
            When a student creates an account
            Then the student is not flagged as Gates-qualified

    Rule: Staff members can decide to flag students as Gates-qualified outside of the study period
        @Gates
        Scenario: Student creates an account when the Gates study feature flag is on
            Given a Gates study feature flag is turned on
            When a student creates an account
            Then the student is flagged as Gates-qualified

        @Gates
        Scenario: Student creates an account within the study period when the Gates study feature flag is off
            Given a Gates study feature flag is turned off
            When a student creates an account
            And the current date is 11/01
            Then the student is flagged as Gates-qualified

        @Gates
        Scenario: Student creates an account outside of the study period when the Gates study feature flag is off
            Given a Gates study feature flag is turned off
            When a student creates an account
            And the current date is 12/20
            Then the student is not flagged as Gates-qualified
