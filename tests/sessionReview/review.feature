Feature: Session review qualification

  As an admin
  I want to know when a session went poorly
  And view such sessions only when action is appropriate

  @SessionReview @Misbehavior
  Scenario Outline: Session flagged for misbehavior does not appear for review until the student has accrued 2 offences
    Given a student who has completed <offences> sessions flagged as <misbehavior>
    When they complete a new session flagged as <misbehavior>
    And an admin visits the review panel
    Then the new session <appears> in the review panel

    Examples: 
      | offences | misbehavior                | appears | 
      | 1        | 'PII'                      | appears | 
      | 2        | 'Pressuring coach'         | appears | 
      | 1        | 'Graded assignment'        | appears | 
      | 1        | 'mean or inappropriate'    | appears |
      | 1        | 'Coach uncomfortable'      | appears |

  @SessionReview @Absent
  Scenario Outline: Session flagged for absent user does not appear for review until the user has accrued 4 offences
    Given a <user> who has completed <offences> session flagged as absent <user>
    When they complete a new session flagged as absent <user>
    And an admin visits the review panel
    Then the new session <appears> in the review panel

  Examples:
    | user      | offences | appears         | 
    | student   | 3        | appears         | 
    | student   | 2        | does not appear | 
    | volunteer | 3        | appears         | 
    | volunteer | 2        | does not appear | 

  @SessionReview @Reported
  Scenario: Sessions flagged as 'reported' always appear for review
    Given a new session flagged as 'reported'
    When an admin visits the review panel
    Then the new session does appear in the review panel

  @SessionReview @LowRating
  Scenario: Sessions flagged as 'Low student rating' always appear for review
    Given a new session flagged as 'Low student rating' after being rated 1-2
    When an admin visits the review panel
    Then the new session does appear in the review panel

  @SessionReview @LowRating
  Scenario: Sessions flagged as 'Low student rating' always appear for review
    Given a new session flagged as 'Low student rating' after being rated 1-2
    When an admin visits the review panel
    Then the new session does appear in the review panel