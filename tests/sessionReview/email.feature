Feature: Session review automated emails

#Sessions definitions: https://docs.google.com/document/d/1k3dgIqs1ZKtyumeAFQI1hNzLujTqHXDF5Um-j3MbmtA/edit
# Updated Docs as of June 2022: https://docs.google.com/document/d/17k0zRVsNeYAAzfbPUVxv4RlZPs-LlX2wK1ZP3Mh-_zQ/edit

Since many outcomes of session reviews by the Programs team consist of sending apology and warning emails
We are automating the delivery of those boilerplate emails

Rule: Warning email for ghosting is sent exactly once
    Scenario: Student ghosts a volunteer for the first time
        Given a student had 0 sessions flagged as 'absent student'
        When student ends a new session flagged as 'absent student'
        Then student should receive a warning email about ghosting volunteers

    Scenario: Student has ghosted a volunteer more than once
        Given a student had 1 session flagged as 'absent student'
        When student ends a new session flagged as 'absent student'
        Then student should not receive a warning email about ghosting volunteers
    
    Scenario: Volunteer ghosts a student for the first time
        Given a volunteer had 0 sessions flagged as 'absent volunteer'
        When student ends a new session flagged as 'absent volunteer'
        Then volunteer should receive a warning email about ghosting students

Rule: Warning email the only first time for student being pushy or just wanting answers
    Scenario: Student pressures volunteer for answers 
        Given a session has been completed
        When the volunteer selects that the student was pressuring them to do their work for them on the post-session form
        Then student should receive a warning email about pressuring volunteer for answers only the first time 

Rule: Apology email is sent only once
    Scenario: Session is unmatched for a student for the first time
        Given a student had 0 unmatched sessions
        When student ends a new unmatched session 
        Then student should receive an apology email

    Scenario: Session has been unmatched for a student more than once
        Given a student had 1 unmatched session
        When student ends a new unmatched session 
        Then student should not receive an apology email

    Scenario: Volunteer gets ghosted for the first time
        Given a volunteer had 0 sessions flagged as 'absent student'
        When student ends a new session flagged as 'absent student'
        Then volunteer should receive an apology email

     Scenario: Volunteer has been ghosted more than once
        Given a volunteer had 1 session flagged as 'absent student'
        When student ends a new session flagged as 'absent student'
        Then volunteer should not receive an apology email   
    
    Scenario: Student gets ghosted for the first time
        Given a student had 0 sessions flagged as 'absent volunteer'
        When student ends a new session flagged as 'absent volunteer'
        Then student should receive an apology email

     Scenario: Student has been ghosted more than once
        Given a student had 1 session flagged as 'absent volunteer'
        When student ends a new session flagged as 'absent volunteer'
        Then student should not receive an apology email   

Rule: Technical info email is sent if 'technical issue' is reported in post session feedback form by either student or volunteer 
    Scenario: Technical issue occurs during session
        Given volunteer or student is on post session feedback form page
        And volunteer or student has selected that there was a technical issue
        When volunteer or student submits the form
        Then student and volunteer both receive an apology email with more info

Rule: Apology email is sent to a volunteer each time they report a student
    Scenario: Volunteer reports a session because student was being rude
        Given volunteer reports the session 
        When the student was being rude
        Then volunteer should get an automatic apology email

     Scenario: Volunteer reports a session because they are concerned about the students well-being
        Given volunteer reports the session 
        When volunteer is concerned about the students well-being
        Then volunteer should get an automatic apology email
