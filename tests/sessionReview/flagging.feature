Feature: Session flagging

  As an admin
  I want to know what happened during sessions
  And have the ability to discern what happened at a glance

  @Flagging @Reported
  Scenario: Reported session is flagged as 'reported'
    Given a student and volunteer in session
    When the volunteer reports the student for any reason
    Then the session is flagged as 'reported'

  @Flagging @Rating
  Scenario Outline: Session with < 3 star rating from student is flagged
    Given a student is filling out post-session survey after a matched <is CC> session
    When the student submits a 2 star review of the <review target>
    Then the session is flagged as <flag>
  
    Examples: 
      | is CC  | review target                 | flag                              | 
      | non-CC | coach                         | 'low coach rating from student'   | 
      | non-CC | session goal                  | 'low session rating from student' | 
      | CC     | any of the coach subquestions | 'low coach rating from student'   | 
      | CC     | the session                   | 'low session rating from student' | 

  @Flagging @Rating
  Scenario: Session with < 3 star enjoyment rating from volunteer is flagged as such
    Given a volunteer is filling out post-session survey after a session
    When the volunteer submits a 1 star rating of their session enjoyment
    Then the session is flagged as 'low session rating from volunteer'

  @Flagging @Absent
  Scenario Outline: Session with absent user is flagged
    Given a student requested a session
    And a volunteer has joined
    And the <user> has sent no messages since the volunteer joined
    And the volunteer has been in the session for <time> mins
    When the session ends
    Then the session is flagged as <flag>
  
    Examples: 
      | user      | flag               | time | 
      | student   | 'absent student'   |  10  |
      | volunteer | 'absent volunteer' |  5   |

  @Flagging @Misbehavior
  Scenario Outline: Session where volunteer reports that student was misbehaving is flagged
    Given a volunteer is filling out post-session survey after a session
    When the volunteer submits the form marked with <misbehavior>
    Then the session is flagged as <misbehavior>

  Examples:
    | misbehavior                            |
    | 'student was only looking for answers' |
    | 'rude or inappropriate'                |