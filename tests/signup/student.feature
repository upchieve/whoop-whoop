Feature: Student signup

  As a student
  I want to sign up for an account
  So that I can get on-demand tutoring

  @Signup @Student @Smoke
  Scenario: Student reaches the sign up page from the login page
    Given an anonymous user
    And the user is on the log in page
    When a user clicks on the sign up button
    Then the user sees the sign up page

  @Signup @Student @Smoke
  Scenario: Student reaches the eligibility form
    Given an anonymous user
    And the user is on the sign up page
    When the user chooses to sign up as a student
    Then the user sees the eligibility page

  @Signup @Student @Smoke
  Scenario: Student searches for and chooses a school there is a match for
    Given an anonymous user
    And the user is on the eligibility page
    When the user selects the school search field
    And types a school name there is a match for
    Then the user should see a list of schools
    And the user should be able to select a school

  @Signup @Student
  Scenario: Student enters an invalid email
    Given an anonymous user
    And the user is on the eligibility page
    When the user enters an invalid email
    And submits the eligibility form
    Then the user sees an invalid email error message

  @Signup @Student
  Scenario: Student enters an invalid zip code
    Given an anonymous user
    And the user is on the eligibility page
    When the user enters an invalid zip code
    And submits the student eligibility form
    Then the user sees an invalid zip code error message

  @Signup @Student
  Scenario: Student enters a previously used email address
    Given an anonymous user
    And the user is on the eligibility page
    When the user fills out the eligibility form with a previously used email address
    And the user submits the eligibility form
    Then the user sees an error indicating they should use a different email address

  @Signup @Student @Smoke
  Scenario: Eligible student fills out the eligibility form
    Given an anonymous user
    And the user is on the eligibility page
    When the user fills out the eligibility form with an eligible location
    And the user submits the eligibility form
    Then the user should see the eligibility confirmation page

  @Signup @Student @Smoke
  Scenario: Eligible student continues sign up process from eligibility form
    Given an anonymous user
    And the user is on the eligibility confirmation page
    When the user chooses to continue
    Then the user sees the student account creation page

  @Signup @Student @Smoke
  Scenario: Student completes account creation
    Given an anonymous user
    And the user is on the student account creation page
    When the user fills out the student account creation form
    And the user checks the checkbox agreeing to our policies
    And the user submits the student account creation form
    Then the user sees the student verification page

  @Signup @Student
  Scenario: Student inputs invalid password
    Given an anonymous user
    And the user is on the student account creation form
    When the user fills out the student account creation form with an invalid password
    And the user checks the checkbox agreeing to our policies
    And the user submits the student account creation form
    Then the user sees an error message explaining our password requirements

  @Signup @Student
  Scenario: Student does not agree to policies
    Given an anonymous user
    And the user is on the student account creation page
    When the user fills out the student account creation form
    And the user does not check the checkbox agreeing to our policies
    And the user submits the student account creation form
    Then the user sees an error message saying they must agree to our policies

  # the VerificationCode tag is here because verification costs money
  # so we don't want to run these tests every time
  @Signup @Student @VerificationCode
  Scenario: Student chooses email verification
    Given a logged in, unverified student
    And the student is on the verification page
    When the student chooses to verify by email
    And the student submits the verification form
    Then the student sees the verification code entry form
    And the student gets a verification email in their inbox

  @Signup @Student @VerificationCode
  Scenario: Student verifies with a correct code
    Given a logged in, unverified student
    And the student has a verification code
    And the student is on the verification code entry page
    When the student enters a correct code
    Then the student sees the verification confirmation page

  @Signup @Student @VerificationCode
  Scenario: Student chooses text verification
    Given a logged in, unverified student
    And the student is on the verification page
    When the student chooses to verify by text message
    And the student enters their phone number
    And the student submits the verification form
    Then the student sees the verification code entry page
    And the student gets a verification code text message

  @Signup @Student @Smoke
  Scenario: Student continues to dashboard from verification confirmation page
    Given a logged in, verified student
    And the student is on the verification confirmation page
    When the student chooses to continue from the verification confirmation page
    Then the student sees the student dashboard

  @Scenario @Student
  Scenario: Student enters an incorrect verification code
    Given a logged in, unverified student
    And the student is on the verification code entry page
    When the student enters an incorrect code
    Then the student sees a verification error message

  @Signup @Student @Logout
  Scenario: Student logs out during the sign up process
    Given a logged in, unverified student
    And the student is on the student verification page
    When the student clicks the log out button
    Then the student sees the logged out page
    And the student is logged out

  @Signup @Student
  Scenario: Student navigates to contact us from the sign up process
    Given an anonymous user
    And the user is on the student account creation page
    When the user clicks the contact us button
    Then the user sees the contact us page

  @Signup @Student
  Scenario: Student navigates to legal policy from sign up process
    Given an anonymous user
    And the user is on the student account creation page
    When the user clicks the legal policy button
    Then the user sees the legal policy page

  @Signup @Student
  Scenario: Student navigates to the main website from the sign up process
    Given an anonymous user
    And the user is on the student account creation page
    When the student clicks the our website button
    Then the student sees the marketing site home page

  Rule: Open students must provide their grade level upon sign up
    @Signup @Student
    Scenario: Open student can specify their grade level
      Given an anonymous user is completing the open student sign up process
      When the user is on the personal info page
      Then there should be an option to specify their grade level

