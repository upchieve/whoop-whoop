Feature: Ineligible Student signup

  As an ineligible student
  I want to know if I am ineligible
  And have the ability to appeal

  @Signup @Student
  Scenario: Ineligible student fills out the eligibility form
    Given an anonymous user
    And the user is on the student eligibility page
    When the user fills out the student eligibility form with an ineligible high school and an ineligible zip code
    And the user submits the student eligibility form
    Then the user sees the ineligibility confirmation page

  @Signup @Student
  Scenario: Ineligible student continues from the ineligible page
    Given an anonymous user
    And the user is on the inelgibile student page
    When the user chooses to continue
    Then the user sees the appeal page

  # @Future represents a story that is desired behavior, but not what site currently does.
  @Signup @Student @Future
  Scenario: Student from outside the US tries to sign up
    Given an anonymous user
    And the user has a non-US IP address
    When the user is on the student sign-up page
    Then the user sees the outside us page

  @Signup @Student
  Scenario: Student searches for a school there is no match for
    Given an anonymous user
    And the user is on the eligibility page
    When the user selects the school search field
    And types a school name there is no match for
    Then the user sees a link to the Can't Find School page

  @Signup @Student
  Scenario: Student visits the Can't Find School page
    Given an anonymous user
    And the user is on the eligibility page
    And the user sees a link to the can't find school page
    When the user clicks the link to the can't find school page
    Then the user sees the can't find school page

    # Ineligible student should always be ineligible based off email, even if new high school or zip code is entered
  @Signup @Student
  Scenario: Ineligible student tries to sign up with eligible high school
    Given an anonymous user
    And the user is on the eligibility page
    When the user fills out the form with a previously used email of an ineligible student
    And the user fills out the form with an eligible high school
    And submits the eligibility form
    Then the user sees the ineligibility confirmation page

  @Signup @Student
  Scenario: Ineligible student tries to sign up with eligible zip code
    Given an anonymous user
    And the user is on the eligibility page
    When the ineligible student fills out the form with a previously used email of an ineligible student
    When the ineligible student fills out the form with an eligible zip code
    And submits the eligibility form
    Then the ineligible student sees the ineligibility confirmation page
