Feature: Partner Student signup

  As a partner student
  I want to sign up for an account via a referral link
  So that I can get on-demand tutoring

  @Signup @Student @Partner
  Scenario: Student visits via a partner referral link
    Given an anonymous user
    When the user visits a student partner referral link
    Then the user sees the student partner sign up page
    And sees the name of the partner on the page

  @Signup @Student @Partner
  Scenario: Partner Student submits partner sign up form part 1
    Given an anonymous user
    And the user is on the student partner sign up page
    When the user selects a site
    And enters personal informaion
    And submits the student partner sign up form
    Then the user sees student partner sign up form part 2

  @Signup @Student @Partner
  Scenario: Partner High school student selects a valid school on student partner sign up form part 2
    Given a student user from a high school partner
    And the student is on student partner sign up form part 2
    And the student sees a checkbox asking if they are a high school student
    When the student checks the box
    And types a school name there is a match for
    Then the user should see a list of schools
    And the user should be able to select a school

  @Signup @Student @Partner
  Scenario: Partner High school student enters an invalid school on student partner sign up form part 2
    Given a student user from a high school partner
    And the student is on student partner sign up form part 2
    And the student sees a checkbox asking if they are a high school student
    When the student checks the box
    And types a school name there is no match for
    Then the user sees a link to further guidance

  @Signup @Student @Partner
  Scenario: Partner high school student submits a valid sign up form part 2
    Given a student user from a high school partner
    And the student is on student partner sign up form part 2
    When the student fills out the form with valid information
    And submits the form
    Then the student sees the student dashboard

  @Signup @Student @Partner
  Scenario: Partner college student submits a valid sign up form part 2
    Given a student user from a college partner
    And the student is on student partner sign up form part 2
    And the student sees a checkbox asking if they are a college student
    When the student fills out the form with valid information
    And submits the form
    Then the student sees the student dashboard
