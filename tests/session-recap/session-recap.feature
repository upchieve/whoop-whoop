Feature: Session Recap

* Tutored Session: A matched session that has a duration of at least 1 minute 

    Rule: Students can view the recap of individual sessions

        Scenario: Student can see the full recap of sessions
            Given a student has at least 1 tutored session in the past 12 months
            When a student visits the session recap page for this session
            Then the student can see the following details about the session
            | detail                                    |
            | subject                                   |
            | date and time                             |
            | duration                                  |
            | coach name                                |
            | coach is favorited or not                 |
            | final read-only state of whiteboard       |
            | entire read-only chat history with coach  |
    
    Rule: Students can view the document editor and whiteboard in session recap only for sessions that required them
    
        Scenario: Student can see the final state of the whiteboard
            Given a student has taken a tutored session in a subject that required a whiteboard
            When the student visits the session recap page for this session
            Then the student will see the final state of the read-only whiteboard
        
        Scenario: Student can see the final state of the document editor 
            Given a student has taken a tutored session in a subject that required a document editor
            When the student visits the session recap page for this session
            Then the student will see the final state of the read-only whiteboard

        Scenario: Student does not see a document editor on the session recap page
            Given a student has taken a tutored session in a subject that did not require a document editor
            When the student visits the session recap page for this session
            Then the student will not see a document editor
        
        Scenario: Student does not see a whiteboard on the session recap page
            Given a student has taken a tutored session in a subject that did not require a whiteboard
            When the student visits the session recap page for this session
            Then the student will not see a whiteboard
        


