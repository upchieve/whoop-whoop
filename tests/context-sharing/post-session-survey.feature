# Source of Truth: https://docs.google.com/document/d/17k0zRVsNeYAAzfbPUVxv4RlZPs-LlX2wK1ZP3Mh-_zQ/edit

Rule: Students will be prompted to take a post session survey at the end of a session
    Scenario: Student ends session
        Given a student and volunteer are in a session
        When student clicks end session
        Then post session survey form shows and student is prompted to fill out survey

    Scenario: Volunteer ends session
        Given a student and volunteer are in a session
        When volunteer clicks end session
        And the student has also chosen to end the session after volunteer
        Then post session survey form shows and student is prompted to fill out survey

Rule: Students will be prompted to rate the session
    Scenario: Pill display for students who rate session 1 or 2 out of 5 stars
        Given a student is filling out the post session survey 
        And student is rating the session
        When student selects 1 or 2 stars 
        Then pills about their experience open and student is prompted to select all that apply

    Scenario: Rating will expand to offer favoriting options for sudents who rate session 4 or 5 out of 5 stars
        Given a student is filling out the post session survey 
        And student is rating the session
        When student selects 4 or 5 stars 
        Then rating will expand to offer the favoriting option

Rule: Content of questions in student survey depend on pre-session survey and vary by subject
    Scenario: content from pre session survey is used in post session survey
        Given a student has completed a session
        When they fill out post session survey
        Then the first question will reference the goal that was selected in the pre session survey

Rule: Volunteer will be prompted to take a post session survey at the end of a session
    Scenario: Student ends session
        Given a student and volunteer are in a session
        When student clicks end session
        And the volunteer has also chosen to end sesion after student 
        Then post session survey form shows and volunteer is prompted to fill out survey

    Scenario: Volunteer ends session
        Given a student and volunteer are in a session
        When volunteer clicks end session
        Then post session survey form shows and volunteer is prompted to fill out survey

Rule: Volunteer will be prompted to rate the session
    Scenario: Pill display for volunteers who rate session 1 or 2 out of 5 stars
        Given a volunteer is filling out the post session survey 
        And volunteer is rating the session
        When volunteer selects 1 or 2 stars 
        Then pills about their experience open and volunteer is prompted to select all that apply

Rule: Content of questions in volunteer survey depend on pre-session survey and vary by subject
    Scenario: content from the studen's pre session survey is used in post session survey
        Given a volunteer has completed a session
        When they fill out post session survey
        Then the first question will reference the goal that the student selected in the pre session survey

Rule: When volunteer is concerned about student they are prompted for more information 
    Scenario: Volunteer selects 'Yes' to having concerns about student they are prompted with reasons 
        Given a volunteer is filling out the post session survey
        When the volunteer selects 'Yes' to having concerns about student
        Then the question will expand with reasons and the volunteer is prompted to select all that apply

Rule: Student and Volunteer can submit survey after 1 question is completed on web 
    Scenario: Volunteer answers 1 questsion 
        Given a volunteer is filling out the post session survey
        When the volunteer answers 1 of the questions
        Then the submit button will be enabled and volunteer can submit survey

    Scenario: Student answers 1 questsion 
        Given a student is filling out the post session survey
        When the student answers 1 of the questions
        Then the submit button will be enabled and student can submit survey

    Scenario: Volunteer does not answer at least 1 questsion 
        Given a volunteer is filling out the post session survey
        When the volunteer hasn't answered any questions
        Then the submit button will be disabled

    Scenario: Student does not answer at least 1 questsion 
        Given a student is filling out the post session survey
        When the student hasn't answered any questions
        Then the submit button will be disabled