Feature: Pre-Session Survey

# Source of truth for copy: https://docs.google.com/document/d/1PP1MpLyS_52UPVMaCdMwFlNmRVRe66DOYo_EOeukvuE/edit

Rule: A Student is asked to fill out pre session survey before starting session

    @StudentPreSessionSurvey
    Scenario: Student clicks start chat and is asked to fill out a pre session survey
        Given a student has an account
        When a student selects "start a chat"
        And a student selects a subjet
        Then a student will be asked to fill out a pre session survey before starting session

Rule: Each question on pre session survey is required

    @StudentPreSessionSurvey
    Scenario: Student has selected an option for current question
        Given a student is using upchieve on the web 
        When the student selects an option on the form for the current question
        Then the "next" buttton will be eneabled 
        And the student proceeds to the next question when "next" button is pressed

    @StudentPreSessionSurvey
    Scenario: Student has not selected an option for current question
        Given a student is using upchieve on the web 
        When the student tries to select "next" button before selectiong option
        Then the "next" button will be disabled
        And the student will remain on the same question

Rule: A student cannot begin the session until filling out the entire survey

    @StudentPreSessionSurvey
    Scenario: Student has completed the survey
        Given a student has answered all the questions on the pre session survey
        When student clicks "start chat" button
        Then the student will begin session

    @StudentPreSessionSurvey
    Scenario: Student has not completed the survey
        Given a student has not answered all the questions on the pre session survey
        When student clicks "start chat" button
        Then the student will remain on survey 
        And "start chat" button is disabled

Rule: the survey is different based on the subject
        
    @StudentPreSessionSurvey
    Scenario: Survey will have differnt questions based on the subject student selected 
        Given a student has started a session for a subject
        When the pre survey form loads
        Then the questions and answer options will differ based on subject selected

Rule: A student can edit their answers before submitting form

    @StudentPreSessionSurvey    
    Scenario: Student edits the survey before submitting it
        Given a student has answerd at least 1 question
        When the student selects "back" button
        Then the student will be taken to the previous question and can select new answer 
