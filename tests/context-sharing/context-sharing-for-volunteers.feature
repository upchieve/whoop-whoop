Feature: Context Sharing for Volunteers

# Source of truth for copy: https://docs.google.com/document/d/1rEdMRkjuyzRDNyQuSYcFcG7VLZg3irAjlSyCTgAY3QQ/edit

Rule: Volunteers can access students' answers to the pre-session survey at any point during a session

    @StudentPreSessionSurvey    
    Scenario: Modal immediately visible upon entering a session
        Given Volunteer is alerted to a session they can take
        When Volunteer joins the session
        Then  Volunteer is immediately presented with a display of the session context 

    @StudentPreSessionSurvey
    Scenario: Modal can be hidden at any point in session
        Given Volunteer is viewing the session context in a session
        When Volunteer hides the session context
        Then The session context disappears

    @StudentPreSessionSurvey
    Scenario: Modal can be shown at any point in session
        Given Volunteer is in a session and has hidden the session context information
        When Volunteer clicks/taps to view the session context
        Then The session context information is viewable

Rule: Volunteers get one single tip depending on the context of the sesssion 

    @StudentPreSessionSurvey
    Scenario: Display tip for confident students on their 1st or 2nd session
        Given Student has never had or has had one "active" session on the platform 
        And the student selects frowning, smiling, or extra big smile emoji for their confidence level when requesting a new session
        When Volunteer joins the session 
        Then The context modal will include the text: "This is [student first name]'s first/second session. Be sure to be welcoming and extra patient as they get used to our platform." 

    @StudentPreSessionSurvey
    Scenario: Don't display tip for confident students requesting their 3rd+ session
        Given Student has had two "active" sessions on the platform 
        And selects frowning, smiling, or extra big smile emoji for their confidence level when requesting a new session
        When Volunteer joins the session
        Then The context modal will not include any tip content

    @StudentPreSessionSurvey
    Scenario: Display tip for students with low confidence on their 1st session or 2nd session
        Given Student has never had or has had one "active" session on the platform 
        And the student selects crying emoji for their confidence level in the pre-session form 
        When Volunteeer joins the session 
        Then The context modal includes the first session tip ("This is [student first name]'s first/second session. Be sure to be welcoming and extra patient as they get used to our platform.")

    @StudentPreSessionSurvey
    Scenario: Display tip for students with low confidence on their 3rd+ session
        Given Student selects crying emoji for their confidence level
        And they have had at least 2 active sessions
        When Volunteer joins the session
        Then The context modal includes the tip content for crying emoji tip. 

Rule: Session context "understanding" and "confidence" changes based for college counseling

    @StudentPreSessionSurvey
    Scenario: Modal will display the "understanding" and "confidence" context for college-counseling session
        Given Student requests a college counseling session
        When Volunteer views the session context
        Then Volunteer sees college-counseling specific session understanding and confidence information

    @StudentPreSessionSurvey
    Scenario: Modal will display the "understanding" and "confidence" context for non college-counseling session
        Given Student requests a non college-counseling session
        When Volunteer views the session context
        Then Volunteer sees the default understanding and confidence information

Rule: The student never sees the context module

    @StudentPreSessionSurvey
    Scenario: Modal will only display for volunteers
        Given Student requests a prealgebra session
        When Student joins the session
        Then Student does not see any session context information

Rule: Session Context has correct pre-session survey data for the subject

    @StudentPreSessionSurvey
    Scenario: Modal displays sudent's goal for prealgebra session
        Given Student requests a prealgebra session
        When Volunteer views the session context
        Then Volunteer sees the goal survey responses from a prealgebra pre-session survey

    @StudentPreSessionSurvey
    Scenario: Modal displays sudent's goal for Reading session
        Given Student requests a reading session
        When Volunteer views the session context
        Then Volunteer sees the goal survey responses from a reading pre-session survey

    @StudentPreSessionSurvey
    Scenario: Modal displays sudent's goal for "Other" session on mobile
        Given Student requests a session from the mobile app
        And chooses "Other" for goal
        When Volunteer views the session context
        Then Volunteer sees a message saying to ask the student about their goal

    @StudentPreSessionSurvey
    Scenario: Modal displays sudent's goal for "Other" session on web
        Given Student requests a session from web
        And chooses "Other" for goal
        When Volunteer views the session context
        Then Volunteer sees the write in response the student wrote for their goal in the pre-session survey