Feature: Session History

* Tutored Session: A matched session that has a duration of at least 1 minute 

    Rule: Students can see the list of past sessions on the session history page

        Scenario: Student sees details about each session on the session history page
            Given a student has at least 1 tutored session in the past 12 months
            When the student visits their session history page
            Then the student can see their most recent sessions and the following details about each session
            | detail                    |
            | subject                   |
            | date and time             |
            | duration                  |
            | coach name                |
            | coach is favorited or not |

        Scenario: Student sees session history only from the past 12 months
            Given a student has at least 1 tutored session in the past 12 months
            When the student visits their session history page
            Then the student can see a list of their tutored sessions from the past 12 months

        Scenario: Student sees their most recent tutored sessions first
            Given a student has at least 1 tutored session
            When the student visits their session history page
            Then the student can see their most recent tutored session first
        
        Scenario: Student sees no sessions on the session history page
            Given a student has at least 1 tutored session
            But 0 tutored sessions in the past 12 months
            When the student visits their session history page
            Then the student sees a message saying that they have not taken any sessions in the past 12 months
    
    Rule: Students can go to session recap from the session history page

        Scenario: Student goes to session recap from session history 
            Given a student is on the session history page
            When the student clicks on a session row
            Then the student will be taken to the session recap page for that session

    Rule: Sessions are displayed through multiple pages

        Scenario: Student sees only 5 sessions at a time
            Given a student has at least 6 tutored sessions
            When the student visits their session history
            Then the student sees the first 5 most recent sessions on page 1

        Scenario: Student sees sessions on page 2
            Given a student has at least 6 tutored sessions
            When the student clicks on the "Next" arrow
            Then student sees a session on page 2

        Scenario: Student can navigate forward a page on session history
            Given a student has at least 6 tutored sessions
            When the student clicks on the "Next" arrow on page 1
            Then the student sees page 2
            
        Scenario: Student can navigate back a page on session history
            Given a student has at least 6 tutored sessions
            When the student clicks on the "Previous" arrow on page 2
            Then the student sees page 1
        
        Scenario: Student sees only the page number of the current page they are on
            Given a student has at least 6 tutored sessions
            When the student visits page 2 of their session history
            Then student can only see "2" as the page number in the page navigation section

        
    
   


        
    


