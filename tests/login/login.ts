import { Given, When, Then } from '@cucumber/cucumber'
import LoginPage from '../../pagemodels/LoginPage'
import StudentDashboardPage from '../../pagemodels/StudentDashboardPage'
import VolunteerDashboardPage from '../../pagemodels/VolunteerDashboardPage'
import SidebarPage from '../../pagemodels/SidebarPage'

Given('a browser is open on the login page', async (t:TestController) => {
  await t.navigateTo('http://localhost:3000/login')
})

When('a student enters valid credentials and submits the form', async () => {
  await LoginPage.fillAndSubmit('student1@upchieve.org', 'Password123')
})

Then('the student sees the student dashboard', async (t:TestController) => {
  await t.expect(StudentDashboardPage.parentComponent.exists).ok()
})

When('a volunteer enters valid credentials and submits the form', async () => {
  await LoginPage.fillAndSubmit('volunteer1@upchieve.org', 'Password123')
})

Then('the volunteer sees the volunteer dashboard', async (t:TestController) => {
  await t.expect(VolunteerDashboardPage.parentComponent.exists).ok()
})

When('an admin enters valid credentials and submits the form', async () => {
  await LoginPage.fillAndSubmit('volunteer1@upchieve.org', 'Password123')
})

Then('the admin sees the volunteer dashboard', async (t:TestController) => {
  await t.expect(VolunteerDashboardPage.parentComponent.exists).ok()
})

Then('the admin sees a link to admin functions', async (t:TestController) => {
  await t.expect(SidebarPage.adminLink.exists).ok()
})

When(/^a user enters invalid "(.+)" and "(.+)"$/, async (t:TestController, [email, password]:string[]) => {
  await LoginPage.fillAndSubmit(email, password)
})

Then('the user sees the invalid credentials error message', async (t:TestController) => {
  await t.expect(LoginPage.loginErrorMessage.exists).ok()
})
