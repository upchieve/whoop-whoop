Feature: Log in

  As a user
  I want to login to my account
  So that I can access the site's functions

  @Login
  Scenario: a student logs in
    Given a browser is open on the login page
    When a student enters valid credentials and submits the form
    Then the student sees the student dashboard

  @Login
  Scenario: a volunteer logs in
    Given a browser is open on the login page
    When a volunteer enters valid credentials and submits the form
    Then the volunteer sees the volunteer dashboard

  @Login
  Scenario: an admin logs in
    Given a browser is open on the login page
    When an admin enters valid credentials and submits the form
    Then the admin sees the volunteer dashboard
    And the admin sees a link to admin functions

  @Login
  Scenario Outline: a user enters invalid credentials
    Given a browser is open on the login page
    When a user enters invalid "<email>" and "<password>"
    Then the user sees the invalid credentials error message

    Examples:
      | email                 | password     |
      | abad@email.com        | abadpassword |
      | abademail             | Password123  |
      | student1@upchieve.org | abadpassword |
