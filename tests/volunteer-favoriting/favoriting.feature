Feature: Volunteer favoriting

    Rule: Students can favorite their coach in a post-session survey

        @VolunteerFavoriting
        Scenario: Student is given an option to favorite their coach in the non-college counseling post-session survey
            Given a student is on the non-college counseling post-session survey
            When the student gives the coach a 4 or 5 rating
            Then the student is given an option to favorite the coach

        @VolunteerFavoriting
        Scenario: Student is given an option to favorite their coach in the college counseling post-session survey
            Given a student is on the college counseling post-session survey
            When the student rates the statement "I would like to receive help from this coach again" as a 4 or 5 rating
            Then the student is given an option to favorite the coach


    Rule: Students cannot favorite their coach once the favoriting limit is reached

        @VolunteerFavoriting
        Scenario: Student is not given an option to favorite their coach in the non-college counseling post-session survey
            Given a student is on the non-college counseling post-session survey
            And has reached the favoriting limit
            When the student gives the coach a 4 or 5 rating
            Then the student is not given an option to favorite the coach

        @VolunteerFavoriting
        Scenario: Student is not given an option to favorite their coach in the college counseling post-session survey
            Given a student is on the college counseling post-session survey
            And has reached the favoriting limit
            When the student rates the statement "I would like to receive help from this coach again" as a 4 or 5 rating
            Then the student is not given an option to favorite the coach
        
        @VolunteerFavoriting
        Scenario: Student cannot favorite a coach from the session recap page
            Given a student has reached the favoriting limit
            When the student visits the session recap page
            Then the student will not be able to favorite another coach from the session recap

        @VolunteerFavoriting
        Scenario: Student cannot favorite a coach from the session history page
            Given a student has reached the favoriting limit
            When the student visits the session history page
            Then the student will not able to favorite another coach from their session history
            

    Rule: Students have favorited coaches shown on their coach page

        @VolunteerFavoriting
        Scenario: Students see a list of favorited coaches in their coach page
            Given a student has favorited coaches
            When the student visits the coach page
            Then the student sees a list of their most recent favorited coaches

    Rule: Students can unfavorite coaches from the coach page

        @VolunteerFavoriting
        Scenario: Student unfavorites a coach from the coach page
            Given a student has favorited coaches
            When the student visits the coach page
            And unfavorites a coach
            Then the student sees an indication that the coach is unfavorited

    Rule: Students are more likely to match with a favorited coach

        @VolunteerFavoriting
        Scenario: Student with favorited coaches requests a session
            Given a student requests a session
            When the student has favorited coaches
            Then the student is more likely to be matched with their favorited coach

    Rule: Students can favorite and unfavorite coaches from the session history page  
        
        @VolunteerFavoriting
        Scenario: Student can favorite a new coach from their session history list
            Given a student has taken at least 1 tutored session
            When the student visits the session history page
            Then the student can favorite a coach from their session history 

        @VolunteerFavoriting
        Scenario: Student can unfavorite a coach from their session history list
            Given a student has favorited a coach
            When the student visits the session history page
            Then the student can unfavorite this coach from their session history

    Rule: Students can favorite and unfavorite coaches from the session recap page  
        
        @VolunteerFavoriting
        Scenario: Student can favorite a new coach from session recap 
            Given a student has taken at least 1 tutored session
            When the student visits a session recap page
            Then the student can favorite a coach from the session recap page

        @VolunteerFavoriting
        Scenario: Student can unfavorite a coach from session recap
            Given a student has favorited a coach
            When the student visits the session recap page for a session with this coach
            Then the student can unfavorite this coach from the session recap page
