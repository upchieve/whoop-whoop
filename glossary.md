# Glossary

## Domain terms

**Gates-qualified student**:

- Not a nonprofit partner student
- Not a school partner student
- In 9th grade or 10th grade
